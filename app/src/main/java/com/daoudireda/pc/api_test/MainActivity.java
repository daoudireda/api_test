package com.daoudireda.pc.api_test;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                // If this is the last question, ends the game.
                // Else, display the next question.
                Intent gameActivity = new Intent(MainActivity.this, PswActivity.class);
                startActivity(gameActivity);
                finish();
            }
        }, 2000); // LENGTH_SHORT is usually 2 second long
    }



}