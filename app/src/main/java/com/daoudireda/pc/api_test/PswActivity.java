package com.daoudireda.pc.api_test;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class PswActivity extends AppCompatActivity implements View.OnClickListener {

    // Les déclarations
    private ImageButton[] img_b = new ImageButton[16];
    private int nb_al;
    private int[] tab_psw = new int[4];
    private int cpt=0;
    private int long_taille =0;
    private int[] saisi_user = new int[4];
    private boolean boul = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psw);

        // Définir un mot de passe par défault
        tab_psw[0]=0;
        tab_psw[1]=0;
        tab_psw[2]=0;
        tab_psw[3]=0;

        // Relation xml avec Java

        img_b[0]=findViewById(R.id.activity_code_B11);
        img_b[0].setTag(0);
        img_b[1]=findViewById(R.id.activity_code_B12);
        img_b[1].setTag(1);
        img_b[2]=findViewById(R.id.activity_code_B13);
        img_b[2].setTag(2);
        img_b[3]=findViewById(R.id.activity_code_B14);
        img_b[3].setTag(3);
        img_b[4]=findViewById(R.id.activity_code_B21);
        img_b[4].setTag(4);
        img_b[5]=findViewById(R.id.activity_code_B22);
        img_b[5].setTag(5);
        img_b[6]=findViewById(R.id.activity_code_B23);
        img_b[6].setTag(6);
        img_b[7]=findViewById(R.id.activity_code_B24);
        img_b[7].setTag(7);
        img_b[8]=findViewById(R.id.activity_code_B31);
        img_b[8].setTag(8);
        img_b[9]=findViewById(R.id.activity_code_B32);
        img_b[9].setTag(9);
        img_b[10]=findViewById(R.id.activity_code_B33);
        img_b[10].setTag(10);
        img_b[11]=findViewById(R.id.activity_code_B34);
        img_b[11].setTag(11);
        img_b[12]=findViewById(R.id.activity_code_B41);
        img_b[12].setTag(12);
        img_b[13]=findViewById(R.id.activity_code_B42);
        img_b[13].setTag(13);
        img_b[14]=findViewById(R.id.activity_code_B43);
        img_b[14].setTag(14);
        img_b[15]=findViewById(R.id.activity_code_B44);
        img_b[15].setTag(15);

        // Affichage aléatoire
        nb_al = (int)(Math.random()*4);

        if(nb_al==0 ||nb_al==1 || nb_al==3)
        {
            img_b[1].setTag(3);
            img_b[1].setBackgroundResource(R.drawable.avocado);
            img_b[3].setTag(1);
            img_b[3].setBackgroundResource(R.drawable.bananas);

            img_b[5].setTag(7);
            img_b[5].setBackgroundResource(R.drawable.mango);
            img_b[7].setTag(5);
            img_b[7].setBackgroundResource(R.drawable.grapes);

            img_b[9].setTag(11);
            img_b[9].setBackgroundResource(R.drawable.pomegranate);
            img_b[11].setTag(9);
            img_b[11].setBackgroundResource(R.drawable.palm);

            img_b[13].setTag(15);
            img_b[13].setBackgroundResource(R.drawable.apricot);
            img_b[15].setTag(13);
            img_b[15].setBackgroundResource(R.drawable.strawberry);
        }
        if (nb_al==2 || nb_al==4)
        {
            img_b[0].setTag(2);
            img_b[0].setBackgroundResource(R.drawable.cherry);
            img_b[2].setTag(0);
            img_b[2].setBackgroundResource(R.drawable.apple);

            img_b[4].setTag(6);
            img_b[4].setBackgroundResource(R.drawable.lemon);
            img_b[6].setTag(4);
            img_b[6].setBackgroundResource(R.drawable.acorn);

            img_b[8].setTag(10);
            img_b[8].setBackgroundResource(R.drawable.pear);
            img_b[10].setTag(8);
            img_b[10].setBackgroundResource(R.drawable.orange);

            img_b[12].setTag(14);
            img_b[12].setBackgroundResource(R.drawable.watermelon);
            img_b[14].setTag(12);
            img_b[14].setBackgroundResource(R.drawable.pineapple);
        }

        while(cpt<16)
        {
            img_b[cpt].setOnClickListener(this);
            cpt++;
        }




    }

    @Override
    public void onClick(View view) {
        // Récupérer le tag du click avec la variable tag
        int tag = (int) view.getTag();
        Toast toast = Toast.makeText(this,"",Toast.LENGTH_SHORT);
        TextView text_v = toast.getView().findViewById(android.R.id.message);
        text_v.setText("OK");
        toast.show();

        if(long_taille!=4)
        {
            saisi_user[long_taille]=tag;
            long_taille++;
        }
        if(long_taille==4)
        {

            // vérifier le mot de passe
            for(cpt=0;cpt<4;cpt++)
            {
                if(saisi_user[cpt]!=tab_psw[cpt])boul = false;
            }

            if(boul)
            {
                text_v.setText("Code correct");
                Intent homeActivity = new Intent(PswActivity.this, HomeActivity.class);
                startActivity(homeActivity);
                finish();
            }
            else
            {
                text_v.setText("Code incorrect");
                long_taille=0;
            }
            toast.show();
        }

    }
}
